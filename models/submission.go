package models

import (
	"github.com/google/uuid"
	"time"
)

type Submission struct {
	ID        uuid.UUID `json:"id"`
	StudentID uuid.UUID `json:"student_id"` // The ID of the student sending the submission
	Date      time.Time `json:"date"`       // The date the question was created
	QuizID    uuid.UUID `json:"quiz_id"`    // The id of the quiz associated with the submission
	Answer    string    `json:"answer"`     // This is a string instead of a rune to allow for different submission types in the future
}
