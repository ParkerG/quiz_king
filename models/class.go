package models

import "github.com/google/uuid"

type Class struct {
	ID           uuid.UUID `json:"id"`
	Title        string    `json:"title"`
	InstructorID uuid.UUID
}
