package models

import (
	"github.com/google/uuid"
	"quiz_king/common"
	"time"
)

type TrueFalseQuiz struct {
	ID           uuid.UUID       `json:"id"`
	ClassID      uuid.UUID       `json:"class_id"`      // The class that the quiz was created for
	InstructorID uuid.UUID       `json:"instructor_id"` // The name of the instructor who created the quiz
	Date         time.Time       `json:"date"`          // The date the quiz was created
	Question     string          `json:"question"`      // The question for the true or false quiz
	Answer       bool            `json:"answer"`        // The answer to the true or false quiz
	TimeLimit    time.Duration   `json:"time_limit"`    // The amount of time students have to answer the questions
	QuizType     common.QuizType `json:"quiz_type"`     // The type of quiz (e.g. Multiple Choice, True or False etc.)
}
