package common

type QuizType string

const (
	MultipleChoice QuizType = "multiple_choice"
	TrueOrFalse    QuizType = "true_or_false"
)
